#!/usr/bin/env python
# coding: utf-8

# In[2]:


#SOAL6
# Given a list of integers, return True if the sequence of numbers 7, 8, 9
# appears in the list somewhere.
# For example:
# arrayCheck([1, 7, 8, 9, 1]) → True
# arrayCheck([1, 9, 7, 8, 1]) → False
# arrayCheck([1, 1, 2, 7, 8, 9]) → True
def arrayCheck(nums):
    a = nums.index(7)
    b = nums.index(8)
    c = nums.index(9)
    if a<b and a<c and b<c:
        return True
    else:
        return False
print(arrayCheck([1, 7, 8, 9, 1]))
print(arrayCheck([1, 9, 7, 8, 1]))
print(arrayCheck([1, 1, 2, 7, 8, 9]))


# In[1]:





# In[ ]:




