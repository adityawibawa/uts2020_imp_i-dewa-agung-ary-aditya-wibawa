#!/usr/bin/env python
# coding: utf-8

# In[7]:


#SOAL12
# 1. The computer will think of 3 digit number that has no repeating
#digits.
# 2. You will then guess a 3 digit number
# 3. The computer will then give back clues, the possible clues are:
#
# Close: You've guessed a correct number but in the wrong position
# Match: You've guessed a correct number in the correct position
# Nope: You haven't guess any of the numbers correctly
#
# 4. Based on these clues you will guess again until you break the code
#with a
# perfect match!
# There are a few things you will have to discover for yourself for this
#game!
# Here are some useful hints:
# Try to figure out what this code is doing and how it might be useful to
#you
import random
digits = list(range(10))
random.shuffle(digits)
question = digits[:3]
print(question)
guessList = []
nowPoint = 0
# Another hint:
while nowPoint<3:
    guess = int(input("What is your guess? "))
    if guess == question[nowPoint]:
        point+=1
        print("Match")
        nowPoint+=1
        continue
    elif guess in question:
        print("Close!")
        continue
    else:
        print("Nope")
        continue
else:
    print("perfect Match!")
  
# Think about how you will compare the input to the random number, what
#format
# should they be in? Maybe some sort of sequence? Watch the Lecture video
#for more hints!


# In[5]:





# In[ ]:




