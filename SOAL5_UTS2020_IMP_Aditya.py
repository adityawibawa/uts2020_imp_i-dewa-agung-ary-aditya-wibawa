#!/usr/bin/env python
# coding: utf-8

# In[1]:


#SOAL5
# You are given two variables:
age = 4
name = "Sammy"
money_in_pocket=100
money_in_bank=500
# Use print formatting to print the following string:
#"Hello my dog's name is Sammy and he is 4 years old"
#"Total Sammy’s money is 600 USD"
print("Hello my dog's name is {} and he is {} years old".format(name,age))
print("Total {}’s money is {} USD".format(name,money_in_pocket+money_in_bank))


# In[ ]:




