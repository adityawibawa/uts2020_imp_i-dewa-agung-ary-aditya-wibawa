#!/usr/bin/env python
# coding: utf-8

# In[2]:


#SOAL9
# Given a string, return a string where for every char in the original,
# there are three chars.
# tripleChar('The') → 'TTThhheee'
# tripleChar('AAbb') → 'AAAAAAbbbbbb'
# tripleChar('Hi-There') → 'HHHiii---TTThhheeerrree'
def tripleChar(str):
    b=''
    for i in str:
        b+=i*3
    return b
print(tripleChar('The'))
print(tripleChar('AAbb'))
print(tripleChar('Hi-There'))


# In[ ]:




