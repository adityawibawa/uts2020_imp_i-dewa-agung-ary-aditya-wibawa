#!/usr/bin/env python
# coding: utf-8

# In[10]:


#SOAL7
# Given a string, return a new string that remove same characters that
# located in their adjacent, so "Hello" yields "Heo".
# For example:
# stringBits('Heelloo') → 'H'
# stringBits('Hi') → 'Hi'
# stringBits('Heeolo') → 'Holo'
# stringBits('Greeeat') → 'Grat'
def stringBits(str):
    str1 = list(str)
    newResult = ''
    for i in str1:
        if str1.index(i)>0 and str1.index(i)<len(str)-1:
            if i!=str1[str1.index(i)+1] and i!=str1[str1.index(i)-1]:
                newResult+=i
        if str1.index(i)==0:
            if i!=str1[str1.index(i)+1]:
                newResult+=i
        if str1.index(i)==len(str)-1:
            if i!=str1[str1.index(i)-1]:
                newResult+=i
    return newResult
print(stringBits('Heelloo'))
print(stringBits('Hi'))
print(stringBits('Heeolo'))
print(stringBits('Greeeat'))


# In[ ]:




