#!/usr/bin/env python
# coding: utf-8

# In[2]:


#SOAL8
# Given two strings, return True if either of the strings appears in the
# beginning of the other string, ignoring upper/lower case differences (in
# other words, the
# computation should not be "case sensitive").
# Note: s.lower() returns the lowercase version of a string.
# Examples:
# begin_other('abcKopi', 'abc') → True
# begin_other('AbC', 'HiaBc') → False
# begin_other('abc', 'abcXabc') → True
# begin_other('abc', 'abXabc') → False
def begin_other(a, b):
    y = b.lower()
    x = a.lower()
    if len(x)>len(y):
        return x.startswith(y)
    elif len(y)>len(x):
        return y.startswith(x)
print(begin_other('abcKopi', 'abc'))
print(begin_other('AbC', 'HiaBc'))
print(begin_other('abc', 'abcXabc'))
print(begin_other('abc', 'abXabc'))


# In[ ]:




