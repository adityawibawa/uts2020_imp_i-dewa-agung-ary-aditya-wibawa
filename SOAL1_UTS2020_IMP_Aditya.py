#!/usr/bin/env python
# coding: utf-8

# In[3]:


#SOAL1
# Given the string:
s = 'prasetiya mulya'
# Use indexing to print out the following:
# 'm'
# 'pra'
# 'semu'
# 'jan'
# 'ayumi'
# 'almaieap'
print(s[-5])
print(s[:3])
print(s[3:5]+s[10:12])
#untuk yang jan tidak bisa menggunakan indexing karena 'j' dan 'n' tidak ada pada string s
print("jan")
print(s[-1]+s[-2]+s[-4]+s[-5]+s[6])
print(s[-1]+s[-3]+s[-5]+s[-1]+s[6]+s[4]+s[-1]+s[0])


# In[ ]:




