#!/usr/bin/env python
# coding: utf-8

# In[1]:


#SOAL11
# Return the number of odd integers in the given array.
# Examples:
# count_odds([2, 1, 2, 3, 4]) → 2
# count_odds([2, 2, 0]) → 0
# count_odds(1, 3, 5]) → 3
def count_odds(nums):
    countOdds = 0
    for i in nums:
        if i%2!=0:
            countOdds+=1
    return countOdds
print(count_odds([2, 1, 2, 3, 4]))
print(count_odds([2, 2, 0]))
print(count_odds([1, 3, 5]))


# In[ ]:




